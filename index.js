/**
 *  @projectName: tvc.logger
 *  @version: 1.0.4
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: index.js
 *  @date: 6/29/2015
 */

//TODO add function definitions

module.exports = function(config) {
    return require("./lib")(config);
};