# tvc.logger #

I created this logger as a solution to a couple issues I was having with some projects.

### What is this repository for? ###

* Logging of any events through simple methods
* Current Version : 1.0.4

### How do I get set up? ###

#### Installation ####
```
#!console

npm install tvc.logger
```
#### Templates #####
```
#!javascript

/**
 * Method template - Standard
 * @param type {String} - Standard types : debug, info, warning, error, critical, fatal
 * @param message {String | Object} - the message to display (will JSON.stringify())
 * @param function {String} - function name to include in output
 * @param console {Boolean} - output to console (default: true)
 * @param status_code {Int} - http status code to include
 */
log[type]([message],[function],[console],[status_code]);

/**
 * Method template - Custom
 * @param type {String} - Custom log type to output
 * inherits other arguments from the standard template
 */
log.custom([type], [message], [function], [console], [status_code]);

// Alternate Argument Passing as object
log[type]({message: {String}, function: {String}, console: {Boolean}, status_code: {Int});
```

#### Example ####
```
#!javascript

var log = require('tvc.logger')({config: settings});

log.info('Logger initiated');
// Other examples
log.debug('Debug message');
```
Will output
```
#!console

2015-06-30 09:44:48.855 - INFO     - Logger Initiated
2015-06-30 09:44:48.855 - DEBUG    - Debug message
```

#### Configuration ####
Full configuration contained in lib/index.js

### More documentation to come ###