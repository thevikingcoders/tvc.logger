var mongoose = require('mongoose');

var log = require('tvc.logger')({toMongo: true, logModel: 'tvc.logger'}).Log;

mongoose.connect("mongodb://bbs-server:27017/thevikingcoder", function(err) {
    if(err) {
        console.log(err);
    } else {
        log.info('Connected to MongoDB');
    }
});

log.error({message: "App Initiated", status_code: 200});