/**
 *  @projectName: tvc.logger
 *  @version: 1.0.0
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: lib/file/file.js
 *  @date: 6/29/2015
 */

// Script dependencies
var path = require('path');
var mkdirp = require('mkdirp');
var fs = require('fs');
var moment = require('moment');
var jf = require('jsonfile');
var q = require('q');
var _ = require('lodash');

// Default variables
var appPath = path.dirname(require.main.filename);
var defaultPath = path.join(appPath, 'logs');

// Configurable variables
var defaultVariables = {
    "extension": 'txt',
    "tsFormat": 'HH:mm:ss.SSS',
    "logFormat": 'text',
    "logSeparator": ' - ',
    "path": defaultPath,
    "filenameFormat": 'YYYY-MM-DD',
    "logPath": null
};

function exists(obj) {
    try {
        fs.statSync(obj);
        return true;
    } catch (err) {
        return err && err.code === "ENOENT" ? false : true;
    }
}

function normalizeExt(ext) {
    if(ext.slice(0, 1) !== '.') {
        return '.' + ext;
    }
    return ext;
}

function normalizePath(p) {
    if(p.slice(-1) !== path.sep) {
        return p += path.sep;
    }
    return p;
}

module.exports = function(LogBase, options) {

    // Use logger base class
    var log = LogBase;
    // Update Configurable Variables
    if(options) {
        _.forEach(options, function (value, name) {
            if (_.keys(defaultVariables).indexOf(name) > -1) {
                defaultVariables[name] = value;
            }
        });
    }

    var fileExp = {};

    fileExp.write = function(msg) {
        genFile()
            .then(function(file) {
                console.log(file);
                if(defaultVariables.logFormat == 'json') {
                    var current = jf.readFileSync(file);
                    current.push(msg);
                    jf.writeFile(file, current, function(err) {
                        if(err) {
                            log.error(err, 'file.write');
                            return false;
                        }
                        return true;
                    });
                } else {
                    msg += '\r\n';
                    fs.appendFile(file, msg, function(err) {
                        if(err) {
                            log.error(err, 'file.write');
                            return false;
                        }
                        return true;
                    });
                }
            }, function() {
                console.log('genFile failed');
                return false;
            });
    };

    function checkPath() {

        var p = defaultVariables.path,
            d = q.defer();

        if(p.indexOf('~') > -1) {
            var pX = p.split('~'),
                pathArray = [];
            pathArray.push(pX[0]);
            if(pX[1].indexOf('|') > -1) {
                pX = pX[1].split('|');
                _.forEach(pX, function(part) {
                    pathArray.push(moment().format(part));
                });
            }
            p = pathArray.join(path.sep);
        }

        if(!exists(p)) {
            if(p.indexOf(appPath) == -1) {
                p = p.join(appPath, p);
            }
            mkdirp(p, function(err) {
                if(err) {
                    console.log(log.msgStdOut('error', 'Error creating path : ' + p + ' - ' + err));
                    d.reject(true);
                } else {
                    defaultVariables.logPath = normalizePath(p);
                    log.info('Path created : ' + defaultVariables.logPath);
                    d.resolve(true);
                }
            })
        }

        return d.promise;

    }

    function genError(file, err) {
        console.log(log.msgStdOut('error', 'Error creating file : ' + file + ' - ' +  err ));
    }

    function genFile() {
        var file;
        console.log('gen file');
        var d = q.defer();

        if(!defaultVariables.logPath) {
            checkPath()
                .then(function() {
                    doWrite(d);
                }, function() {
                    return false;
                })
        } else {
            doWrite(d);
        }

        function doWrite(d) {
            //file = moment().format(defaultVariables.filenameFormat) + normalizeExt(defaultVariables.extension);
            file = 'today' + normalizeExt(defaultVariables.extension);
            file = path.join(defaultVariables.logPath, file);
            console.log(file);

            if(!exists(file)) {
                if(defaultVariables.logFormat == 'json') {
                    jf.writeFile(file, [], function(err) {
                        if(err) {
                            genError(file, err);
                            d.reject(true);
                        } else {
                            d.resolve(file);
                        }
                    })
                } else {
                    fs.closeSync(fs.openSync(file, 'w'));
                    if(defaultVariables.logFormat == 'csv') {
                        var header = ['timestamp', 'log_type', 'log_message', 'log_function'];
                        header = '"' + header.join('"' + defaultVariables.logSeparator + '"') + '"\r\n';
                        fs.appendFile(file, header, function(err) {
                            if(err) {
                                console.log(log.msgStdOut('error', `Error adding header to ${file} - ${err}`, null, true));
                            }
                            d.resolve(file);
                        });
                    } else {
                        d.resolve(file);
                    }
                }
            } else {
                d.resolve(file);
            }

        }

        return d.promise;

    }

    return fileExp;

};