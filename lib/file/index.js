/**
 *  @projectName: tvc.logger
 *  @version: 1.0.0
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: lib/file/index.js
 *  @date: 6/29/2015
 */

module.exports = function(log, config) {
    return {
        "File": require("./file")(log, config)
    }
};