/**
 *  @projectName: tvc.logger
 *  @version: 1.0.2
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: lib/index.js
 *  @date: 6/30/2015
 */

/**
 * Including Module
 * var Log = require('./tvc.mean.logger')(config).Log;
 *
 * Examples
 * @example Log.debug("debug message", "Log.debug");
 * @example Log.info("info message", "Log.info");
 * @example Log.warning("warning message", "Log.warning");
 * @example Log.error("error message", "Log.error");
 * @example Log.critical("critical message", "Log.critical");
 * @example Log.fatal("fatal message", "Log.fatal");
 * @example Log.custom("custom", "custom message", "Log.custom");
 *
 * Standard Configuration
 * @config console {Boolean} Output log to console | default : true
 * @config ts_format {String} MomentJS Timestamp format | default : 'YYYY-MM-DD HH:mm:ss.SSS'
 * @config log_separator {String} concatenation string for file logging | default : ' - '
 * @config padding {Number} Right padding for console output for log type | default : 8
 *
 * Database Logging
 * @config toMongo {Boolean} write log to MongoDB | default : false | Requires an active mongoose connection
 *  @override log_format {String} default : json
 * @config logModel {String} Model name for log collection | default : 'tvc.log.prime'
 *
 * Not editable through config
 * @config appPath {String} NodeJS logical basepath | default : path.dirname(require.main.filename)
 *
 * File Logging
 * @config toFile {Boolean} write log to a file | default : false
 * @config log_format {String} format of logging output for file | default : text
 *  @valid @option ['text', 'json', 'csv']
 *  @log_format @option csv
 *    @override padding {Number} default : 0
 *    @override ts_format {String} default : 'YYYY-MM-DD HH:mm:ss'
 *    @override log_separator {String} default : ','
 *    @override extension {String} default : 'csv'
 * @config extension {String} file extension for file logging | default : txt
 * @config path {String} File log path | default : {appPath}/log
 * @config filename_format {String} MomentJS Date Format | default : 'YYYY-MM-DD'
 *
 * @returns {Object} {Log: *}
 */

var _ = require('lodash');
var moment = require('moment');

var defaultVars = {
    "toFile": false,
    "toMongo": false,
    "logFormat": 'text',
    "logSeparator": ' - ',
    "defaultSep": ' - ',
    "extension": 'txt',
    "tsFormat": 'YYYY-MM-DD HH:mm:ss.SSS',
    "console": true,
    "padding": 8,
    "mongoLog": null,
    "fileLog": null
};

module.exports = function(options) {

    if(options) {
        _.forEach(options, function (value, name) {
            if(_.keys(defaultVars).indexOf(name) > -1) {
                defaultVars[name] = value;
            }
        });
    }

    if(defaultVars.toMongo) {
        var mongoLog = require('./mongoose')(options).Mongo;
        defaultVars.logFormat = 'json';
    }

    if(defaultVars.logFormat == 'csv') {
        defaultVars.padding = 0;
        defaultVars.tsFormat = options['tsFormat'] || 'YYYY-MM-DD HH:mm:ss';
        defaultVars.logSeparator = options['logSeparator'] || ',';
    }

    var logBase = {};

    logBase.msgStdOut = msgStdOut;

    logBase.debug = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('debug', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('debug', _message, _function, _console, _status);
        }
    };

    logBase.info = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('info', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('info', _message, _function, _console, _status);
        }
    };

    logBase.warning = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('warning', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('warning', _message, _function, _console, _status);
        }
    };

    logBase.error = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('error', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('error', _message, _function, _console, _status);
        }
    };

    logBase.critical = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('critical', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('critical', _message, _function, _console, _status);
        }
    };

    logBase.fatal = function(_message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg('fatal', _message);
        } else {
            _console = _console || defaultVars.console;
            doLog('fatal', _message, _function, _console, _status);
        }
    };

    logBase.custom = function(_type, _message, _function, _console, _status) {
        if(typeof _message == "object") {
            parseMsg(_type, _message);
        } else {
            _console = _console || defaultVars.console;
            doLog(_type, _message, _function, _console, _status);
        }
    };

    if(defaultVars.toFile) {
        var fileLog = require('./file')(logBase, options).File;
    }

    function doLog (_type, _message, _function, _console, _status) {
        var msg;
        if(defaultVars.logFormat == 'json') {
            msg = {
                log_type: _type,
                log_message: _message
            };
            if(_function) {
                msg['log_function'] = _function;
            }
            if(_status) {
                msg['log_status_code'] = _status;
            }
        } else {
            msg = msgStdOut(_type, _message, _function, _status);
        }
        if(defaultVars.toMongo) {
            mongoLog.write(msg);
        }
        if(defaultVars.toFile) {
            fileLog.write(msg);
        }
        if(_console) {
            console.log(msgStdOut(_type, _message, _function, _status, true));
        }
    }

    function msgStdOut(_type, _message, _function, _status, default_sep) {
        var ts = moment().format(defaultVars.tsFormat);

        var msg = [];

        msg.push(ts);
        msg.push((_type + '        ').slice(0, defaultVars.padding).toUpperCase());
        msg.push(_message);
        if(_function) {
            msg.push(_function);
        }
        if(_status) {
            msg.push('Status Code : ' + _status);
        }

        if(default_sep) {
            return msg.join(defaultVars.defaultSep).replace(/"/g, '');
        }

        if(defaultVars.logFormat == 'csv') {
            return '"' + msg.join('"' + defaultVars.logSeparator + '"') + '"';
        }

        return msg.join(defaultVars.logSeparator);
    }

    function parseMsg(_type, msg) {
        var _message = null,
            _function = null,
            _console = defaultVars.console,
            _status = null;
        if(typeof msg == "object") {
            if(msg['message']) {
                _message = msg['message'];
                if(typeof _message == "object") {
                    _message = JSON.stringify(_message);
                }
            }
            if(msg['function']) {
                _function = msg['function'];
            }
            if(msg['console_out']) {
                _console = msg['console_out'];
            }
            if(msg['status_code']) {
                _status = msg['status_code'];
            }
        }
        if(!_type || !_message) {
            console.log('Improper formed message object  : ' + JSON.stringify(msg));
        }
        doLog(_type, _message, _function, _console, _status);
    }

    return logBase;

};