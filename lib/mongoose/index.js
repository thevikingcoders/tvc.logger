/**
 *  @projectName: tvc.logger
 *  @version: 1.0.0
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: lib/mongoose/index.js
 *  @date: 6/29/2015
 */

module.exports = function (config) {
    return {
        "Mongo": require("./mongoose")(config)
    }
};