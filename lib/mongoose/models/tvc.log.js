/**
 *  name: tvc.logger
 *  version: 1.0.0
 *  author: Bill Brady <bill@thevikingcoder.com>
 *  file: models/tvc.log.js
 *  date: 6/29/15
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var LogPrimarySchema = new Schema({
    "log_dt": {"type": Date, "default": Date.now},
    "log_type": {"type": String},
    "log_status_code": {"type": Number},
    "log_function": {"type": String},
    "log_message": {"type": String},
    "viewed": {"type": Boolean, "default": false}
});

module.exports = function (logModel) {
    var prime = logModel || 'tvc.log.prime';
    return {
        "Prime": mongoose.model(prime, LogPrimarySchema)
    }
};