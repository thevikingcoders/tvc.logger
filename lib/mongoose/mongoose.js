/**
 *  @projectName: tvc.logger
 *  @version: 1.0.0
 *  @author: Bill Brady <bill@thevikingcoder.com>
 *  @file: lib/mongoose/mongoose.js
 *  @date: 6/29/2015
 */

var moment = require('moment');
var _ = require('lodash');
var q = require('q');

var defaultVars = {
    "logModel": "tvc.logs.prime",
    "tsFormat": 'YYYY-MM-DD HH:mm:ss.SSS'
};

module.exports = function (options) {

    if(options) {
        _.forEach(options, function(value, name) {
            if(_.keys(defaultVars).indexOf(name) > -1) {
                defaultVars[name] = value;
            }
        });
    }

    var model = require('./models/tvc.log')(defaultVars.logModel);

    function ts() {
        return moment().format(defaultVars.tsFormat);
    }

    var mongoLog = {};

    mongoLog.write = function(doc) {
        var newLog = new model.Prime(doc);

        newLog.save(function(err) {
            if(err) {
                console.error(ts() + ' - ' + err);
            }
        })
    };

    mongoLog.getLog = function(s_date, e_date) {

        var find = {};

        if(s_date && e_date) {
            find['log_dt'] = {$gte: s_date, $lt: e_date};
        }
        else if (s_date && !e_date) {
            find['log_dt'] = {$gte: s_date};
        }
        else if (!s_date && e_date) {
            find['log_dt'] = {$lt: e_date};
        }

        var d = q.defer();

        model.Prime
            .find(find)
            .sort({log_dt: 1})
            .exec(function(err, docs) {
                if(err) {
                    console.error(ts() + ' - ' + err);
                    d.reject(err);
                } else {
                    if(_.keys(docs).length == 0) {
                        d.reject({msg: 'No records'});
                    } else {
                        d.resolve(docs);
                    }
                }
            });

        return d.promise;
    };

    return mongoLog;

};